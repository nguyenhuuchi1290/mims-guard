import React from "react";
import AppFooter from './components/AppFooter';
import AppMenu from './components/AppMenu';

export default function Root(props) {
  return (
    <React.Fragment>
    <AppMenu />
    <AppFooter />
    </React.Fragment>

  );
}
