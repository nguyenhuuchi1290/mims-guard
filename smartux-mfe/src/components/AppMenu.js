import React from 'react';
import './smartux.lazy.css';
import './PredefinedThemes.scss';
import DeniReactTreeView from "deni-react-treeview";

const SMARTUX_HOST = 'http://localhost:5003';
const themes = [ 'classic', 'metro', 'moonlight', 'purple', 'green', 'orange', 'red', 'silver' ];

const AppMenu = () => (
  <header>
      <div style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }} >
      {
        themes.map((theme, index) => {
          return (
            <DeniReactTreeView
              style={{ marginRight: '10px', marginBottom: '10px' }}
              key={index}
              showCheckbox={true}
              theme={theme}
              url="https://raw.githubusercontent.com/denimar/deni-react-treeview/develop/src/assets/data/countries-by-continents.json"
            />
          )
        })
      }
    </div>
    <div className="left-menu">
        <img className="nav-img" src={SMARTUX_HOST+'/public/smartux_adm/images/admin/m_title_04.gif'} width="180px"/>
        <ul id="browser" className="filetree treeview">
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">관리자</span>
                        <ul>
                            <li className="last">
                                <span className="file">
                                    관리자 리스트
                                </span>
                            </li>
                        </ul>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">세컨드TV 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">코드</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">트리거</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">패널/지면</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">Cache</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">자체편성관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">BestVOD 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">Youtube</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">약관 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">공지 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">Push 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">HDTV</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">VOD 프로모션</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">Paynow</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">별점 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">구매내역</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">배너 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">전용포스터 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">Senior</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">화제동영상 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">포터블 TV관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">고객 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">IPTV 관리</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">가이드채널</span>
			</li>
            <li className="expandable">
                <div className="hitarea expandable-hitarea"></div>
                <span className="folder">AB테스트 관리</span>
			</li>
            
            
        
            


            
        </ul>
    </div>
  </header>
);

export default AppMenu;
