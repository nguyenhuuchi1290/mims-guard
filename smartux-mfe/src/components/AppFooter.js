import React from 'react';
import './smartux.lazy.css';
const SMARTUX_HOST = 'http://localhost:5003';
const AppFooter = () => (
  <footer>
        <div className="copyright">
            <img className="footer-img" src={SMARTUX_HOST+'/public/smartux_adm/images/admin/copyright.gif'} width="400px"/>
        </div>
  </footer>
);

export default AppFooter;
