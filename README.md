# amims-asis

This project consists of:
- amims-mfe:
    + root-config: shared HTML layout for microfrontends
    + navbar: basic Svelte application with navigation responsibilities
    + login: basic React application for logged-out users
    + auth: utility module, using Rxjs and plain JavaScript
- smartux-mfe: smartux application 
- coupon-mfe: coupon application 

# Running locally 
sudo yarn install
sudo yarn start
- amims-mfe:
    + root-config runs on port 9000
    + auth runs on port 9001
    + navbar runs on port 5000
    + login runs on port 5002
- smartux-mfe: runs on port 5003
- coupon-mfe: runs on port 5004

go to http://localhost:9000/
celebrate good times 🎉