/**
 * ́´ë¯¸́§€ í¬ë©§, ́‚¬́´́¦ˆ, width, height́ œí•œ ́¤í¬ë¦½í¸
 */
//---------------́´ë¯¸́§€ ́ œí•œ-----------------//
function browserCheckReset(fileId) {
	//́µ́¤í”Œë¡œëŸ¬ ë²„́ „ ë¹„êµ íŒŒ́¼ ́´ˆê¸°í™”.
	if(/MSIE/.test(navigator.userAgent)){
	    //IE 10.0 ́´í•˜ë©´
	    if(navigator.appVersion.indexOf("MSIE 10.0")>=0 || navigator.appVersion.indexOf("MSIE 8.0")>=0){
	    	$("#"+fileId+"").replaceWith( $("#"+fileId+"").clone(true) );
	    }else{	 //IE 10.0 ́´́ƒ ë˜ë” í¬ë¡­ / íŒŒí­
	    	$("#"+fileId+"").attr("value","");	
 		}
	} else {
		$("#"+fileId+"").attr("value","");	
	}
}

/*
 * ́´ë¯¸́§€ í—¤ë”ë¥¼ í†µí•´ ́´ë¯¸́§€ í¬ë§· / ê°€ë¡œ,́„¸ë¡œ ́‚¬́´́¦ˆ í™•́¸ í›„ json ́‘ë‹µ (ë¹„ë™ê¸°́‹ ajaxë¡œ êµ¬í˜„)
 * fileId : ́²´í¬ í•˜ê³ ́ í•˜ë” input(type=file) ́˜ ID/NAME ê°’
 * propertyNm : í”„ë¡œí¼í‹°́— ́§€́ •í•´ë†“́€ ́‚¬́´́¦ˆ́™€ íƒ€́…, í¬ë§·́„ ́°¾ê¸° ́œ„í•œ í”„ë¡œí¼í‹° ë„¤́„
 */
function imgHeaderCheck(fileId, propertyNm, imgSize) { 
	 
	 //onchange ́´ë²¤í¸ë¡œ ́¸í•´ ́¤‘ë³µ́²´í¬́˜¤ë¥˜́¡´́¬ ́ œ́™¸ ́²˜ë¦¬
	 if ($('#'+fileId).val()=="") {
		 return;
	 }
    /*́´ë¯¸́§€ ́©ëŸ‰ í™•́¸́€ ImageIoë¡œ ́²´í¬ë¶ˆê°€ (́¤í¬ë¦½í¸́—́„œ ́²˜ë¦¬) */    
    var size = document.getElementById(fileId).files[0].size;
	//var maxSize = 102400;
	var maxSize = imgSize;
	var fileSize = Math.round(size);
	
	if(fileSize > maxSize*1024) {
		alert("́´ë¯¸́§€ ́©ëŸ‰́„ ́´ˆê³¼ í–ˆ́µë‹ˆë‹¤.\ńµœëŒ€ ́´ë¯¸́§€ ́‚¬́´́¦ˆ : "+maxSize+"KB");
		browserCheckReset(fileId);
        return;
	}
	
	var formData = new FormData();          
	formData.append("imgPath", $("#"+fileId+"")[0].files[0]); // ́²´í¬í•´́•¼í•  ́²¨ë¶€íŒŒ́¼́„ ë³´ëƒ„   
	formData.append("propertyNm", propertyNm);   // í”„ë¡œí¼í‹°́— ́§€́ •í•´ë†“́€ ́‚¬́´́¦ˆ́™€ íƒ€́…, í¬ë§·́„ ́°¾ê¸° ́œ„í•´ í”„ë¡œí¼í‹° ë„¤́„́„ ́…ë ¥í•œë‹¤. ex) theme.tv.main.width ́˜ ê²½́° theme.tv.main ë§Œ ê°€́ ¸ê°„ë‹¤  
	
	$.ajax({            
		url: '/smartux_adm/admin/commonMng/imgHeaderCheck.do',            
		data: formData,            
		processData: false,            
		contentType: false,            
		type: 'POST',   
		dataType: 'json',
		success: function(data){       
			if(data.result.flag=="0000"){ //́‚¬́´́¦ˆ, í¬ë§·́´ ëª¨ë‘ ë§́œ¼ë©´ PASS
			} else if(data.result.flag=="1001"){ //í¬ë§·́´ ë§́§€ ́•́œ¼ë©´
				alert(data.result.message + "ë” ́´ë¯¸́§€ í¬ë§·́— ë§́§€ ́•́µë‹ˆë‹¤. \n ́ í•©í•œ í¬ë§· í˜•́‹ : " + data.result.format);
				browserCheckReset(fileId);
			} else if(data.result.flag=="2002"){ //́‚¬́´́¦ˆê°€ ë§́§€ ́•́œ¼ë©´
				var sizeType;
				if (data.result.type == "equal") { 
					sizeType = "ê°™́•„́•¼";
				} else if(data.result.type == "max") {
					sizeType = "ê°™ê±°ë‚˜ ́‘́•„́•¼";
				}
				var msg1 = " ́‚¬́´́¦ˆê°€ ë§́§€ ́•́µë‹ˆë‹¤";
				var msg2 = "́µœ́ í™” ́‚¬́´́¦ˆ : " + data.result.width + "px * " + data.result.height + "px";
				var msg3 = "ë³¸ ́´ë¯¸́§€ë” ́µœ́ í™” ́‚¬́´́¦ˆ́™€ "+ sizeType + " í•©ë‹ˆë‹¤.";
				
				if (data.result.message == "width") { //ê°€ë¡œ ́‚¬́´́¦ˆê°€ ë§́§€ ́•́„ ê²½́°,
					alert("ê°€ë¡œ"+msg1+"\n"+msg2+"\n"+msg3);
				} else if (data.result.message == "height") { //́„¸ë¡œ ́‚¬́´́¦ˆê°€ ë§́§€ ́•́„ ê²½́°,
					alert("́„¸ë¡œ"+msg1+"\n"+msg2+"\n"+msg3);
				} else if (data.result.message == "all") { //ê°€ë¡œ*́„¸ë¡œ ëª¨ë‘ ́‚¬́´́¦ˆê°€ ë§́§€ ́•́„ ê²½́°,
					alert("ê°€ë¡œ * ́„¸ë¡œ"+msg1+"\n"+msg2+"\n"+msg3);
				}
				browserCheckReset(fileId);
			}
		}      
	});
}
//--------------------------------------------------------------//