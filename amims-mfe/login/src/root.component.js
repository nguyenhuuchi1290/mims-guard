import React, { useState, useEffect } from "react";
import { auth$, login } from "@example/auth";
import Loader from "./loader.component";
import env from "react-dotenv";
// require('dotenv').config();
export default function Root(props) {
  const [pending, setPending] = useState(false);
  const [error, setError] = useState();

  useEffect(() => {
    let timeout;
    const sub = auth$.subscribe(({ pending, error }) => {
      // redirecting to /home when logged in will be done in the navbar. Cohesive code FTW!
      setPending(pending);
      setError(error);
      timeout = setTimeout(() => {
        setError();
      }, 2000);
    });
    return () => {
      clearInterval(timeout);
      sub.unsubscribe();
    };
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    const { username, password } = document.forms.login.elements;
    login(username.value, password.value);
  };
  const LOGIN_HOST = 'http://localhost:5002';
  // const LOGIN_HOST = env.REACT_LOGIN_HOST;

  return (
    <div>
      <div className="form-center">
      <form name="login" className="login-form" onSubmit={onSubmit}>
        <div className="form-head">
            <img src={LOGIN_HOST+'/public/images/admin/login/IPTV01.gif'} width="270"/>
        </div>
        <div className="break-line-3">
				</div>
        <div className="break-line-1">
				</div>
        <div class="form-body">
							<img className="logo-responsive" src={LOGIN_HOST+'/public/images/admin/login/logo_iptv.png'} />
				</div>
        <div class="form-bottom">
							<div class="form-bottom-center">
								<div class="form-bottom-center-content">
										<img className="user-responsive" src={LOGIN_HOST+"/public/images/admin/login/ID.gif"} />
										<input  id="username" type="text" className="input-user"  maxlength="12" tabindex="1" required/>
										<img className="pass-responsive" src={LOGIN_HOST+"/public/images/admin/login/password.gif"} />
										<input type="password" id="password" className="input-password" maxlength="20" tabindex="2" required />
                    <button className="button-hide" type="submit" disabled={pending}>
                      <img className="button-responsive" src={LOGIN_HOST+"/public/images/admin/login/loginbutton.gif"} />
                      {/* {pending ? <Loader /> : ""} */}
                    </button>
								</div>
                {error && <div className="login-error">{error}</div>}
						</div>
        </div>    
      </form>
    </div>
    </div>
    


    
  );
}
